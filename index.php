 <!DOCTYPE html>
 <html>
 <head>
 	<meta charset="utf-8">
 	<title>10 Últimos Tweets de The Practical Dev</title>
 	<link rel="stylesheet" type="text/css" href="css/estilos.css">
 	<script type="text/javascript" src="js/jquery-v3.1.0.js"></script>

 </head>
 <body>
 	<header>
		<h1>10 Últimos Tweets "ThePracticalDev"</h1>
	</header>
	<section>
		<div class="tweet" onclick="lanzarModal()">
			<img class="img" src=""/>
			<div class="info">
				<p class="user">
					<span id="id" class="id"></span>
					<span class="date"></span>
				</p>
				<p class="text"></p>
				<p class="likes"></p>
			</div>
		</div>
		<article id="tweets"></article>
	</section>

	<div id="myModal" class="modal">

	  <div class="modal-content">
	    <span class="close">&times;</span>
	    <div id="tweets-modal"></div>
	  </div>

	</div>

	<footer>
		<p>
			Trabajo realizado por Luis Alvarado - 2018
		</p>
	</footer>
	<script type="text/javascript" src="js/principal.js"></script>
 </body>

 </html>