<?php

# challenge 1
function getPiDecimal(){
# your code here
	$pi = "".pi(); #echo M_PI;
	$decimales = explode('.', $pi);
	$decimo = $decimales[1];
	return $decimo[9];
}

# challenge 2
function getSumEvens($a=[1,2,3,4,5,6]){
# your code here
	$sum = 0;
	foreach ($a as $p => $valor) {
		if ($valor%2==0) {
			$sum += $valor;
		}		
	}
	return $sum;
}

# challenge 3
function getOrderedVowels($s="just a testing"){
# your code here
	$vocales = "";
	foreach (count_chars($s, 1) as $i => $val) {
		if (preg_match('/[aeiou]/i',chr($i))) {
		        $vocales .= chr($i);
		}					
	}
	return $vocales;
}

# challenge 4
# obtener el primer id del JSON : https://jsonplaceholder.typicode.com/users
function getFirstId(){
# your code here
	$html = file_get_contents('https://jsonplaceholder.typicode.com/users'); 
	$json = json_decode($html); 

	#for($i = 0; $i < count($json); $i++){
	$id = $json[0]->id;
	/*$nombre = $json[$i]->name;
	    $usuario = $json[$i]->username;
	    echo $id." ".$nombre." ".$usuario;
	    echo "";
	}*/
	return $id;
}

echo "***** Ejercicios *****<br>";
echo "1. PI: ".getPiDecimal();
echo "<br>";
echo "2. Suma: ".getSumEvens();
echo "<br>";
echo "3. Ordenar Vocales: ".getOrderedVowels();
echo "<br>";
echo "4. Primer ID: ".getFirstId();
echo "<br>**************<br><br>";


# DONT EDIT
echo "Running: \n";
echo "challenge 1: ".((getPiDecimal()==5)? "pass" : "fail") . "\n" ;
echo "challenge 2: ".((getSumEvens()==12)? "pass" : "fail" ) . "\n" ;
echo "challenge 3: ".((getOrderedVowels()=="uaei")? "pass" : "fail") . "\n" ;
echo "challenge 4: ".((getFirstId()==1)? "pass" : "fail" ).		 "\n" ;
