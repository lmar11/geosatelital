1) que tanto conoces sobre sistemas ERP?
Son sistemas de información que integran y manejan operaciones 
de productividad, logística, contabilidad, etc. 
de una organización con el fin de automatizar sus procesos.

2) que tanto conoces sobre facturacion electronica?
Es un tipo de comprobante de pago emitido por un 
Sistema Electrónico de algún contribuyente.

3) que tanto conoces sobre procesos de CI/CD?
Tiene que ver con automatizar procesos dejando de lado los 
procesos manuales esto hace que las organizaciones sean más competitivas.

4) que tanto conoces sobre linux?
Es un S.O. libre y de código abierto que a mi parecer más ligero que windows, 
versátil, personalización y acceso total al S.O. para adaptarlo según las necesidades.

5) que te gusta y NO te gusta del lenguaje que usas?
Bueno el lenguaje que me gusta para desarrollo web es 
PHP por sus entornos de desarrollo rápidos y fáciles de configurar 
y su fácil acceso a base de datos, 
también de que es un lenguaje libre y de código abierto.

Y lo que no me gusta de PHP es de que si no lo configuras 
adecuadamente dejas abiertas muchas brechas de seguridad 
aparte de que hay que instalar un servidor web.

6) si llegas a un punto con algo que no sabes como solucionar que haces?
Siempre trato de investigar (libros, web, etc.) y de cualquier manera encontrar solución.

7) que opinas del trabajo remoto, tienes experiencia?
No tengo experiencia en ello pero sé que remotamente no hay necesidad de tener que 
ir a ún determinado lugar para hacer algo sino desde 
el punto en que te encuentres y con una conexión a internet 
ya lo puedes hacer y eso involucraría reducir tiempo y costos
 además de que te permite trabajar comodamente en el lugar que quieras.

8) si te dan una instruccion que no comprendes que haces?
Pregunto nuevamente que me indique más detallado que es lo que debo de hacer.

9) que opinas de las "horas extras"?
No tengo ningún problema cocn las horas siempre y cuando el esfuerzo sea bien recompenzado.

10) cuentame cual es la cosa mas dificil que has resuelto.
Implementar un red neuronal artificial que reconozca números, letras y signos lo hice en JAVA.